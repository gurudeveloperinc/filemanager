<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initialdb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('categories', function (Blueprint $table) {
            $table->increments('catid');
            $table->string('name');
            $table->timestamps();
        });

        //Document

        Schema::create('docs', function (Blueprint $table) {
            $table->increments('docid');
            $table->integer('uid');
            $table->string('name');
            $table->string('description');
            $table->integer('catid');
            $table->timestamps();
        });



        Schema::create('images', function (Blueprint $table){
           $table->increments('imid');
           $table->integer('docid');
           $table->string('url',3000);
           $table->timestamps();

        });



        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email');
            $table->string('token');
            $table->timestamp('created_at');
        });


        Schema::create('users', function (Blueprint $table) {
            $table->increments('uid');
//            $table->string('staffid');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('users');
        Schema::dropIfExists('docs');
        Schema::dropIfExists('categories');
        //
    }
}
