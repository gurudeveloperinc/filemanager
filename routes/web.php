<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('create-doc', 'HomeController@getCreateDoc');

Route::get('create-cat', 'HomeController@getCreateCat');




Route::post('create-cat', 'HomeController@postCreateCat');
Route::post('create-doc', 'HomeController@postCreateDoc');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/{id}', 'HomeController@viewDoc');
