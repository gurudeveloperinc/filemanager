<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class images extends Model
{
    protected $primaryKey = 'imid';


    public function document()
    {
        return $this->belongsTo('App\document', 'docid');
    }
}
