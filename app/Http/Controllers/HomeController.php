<?php

namespace App\Http\Controllers;

use App\category;
use App\document;
use App\images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use PhpParser\Comment\Doc;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Input::has('search')){

            $search = Input::get('search');

            $docs = document::where('name', 'like', "%$search%" )->get();
            $cats = category::all();
        } else{
            $docs = document::all();
            $cats = category::all();
        }


        return view('home',[
            'docs'=> $docs,
            'cats' => $cats
        ]);
    }


    public function getCreateCat()
    {
        return view('createCat');
    }


    public function postCreateCat(Request $request)
    {
        $cat = new category();
        $cat->name = $request->input('name');
        $cat->save();

        return redirect('/create-cat');
    }
    
    
    
    

    public function getCreateDoc()
    {
    	$categories = category::all();
        return view('createDoc',[
        	'categories' => $categories
        ]);

    }



    public function postCreateDoc(Request $request)
    {
        $doc = new document();
        $doc->uid = Auth::user()->uid;
        $doc->catid = $request->input('catid');
        $doc->name = $request->input( 'name');
        $doc->description = $request->input('description');
        $docStatus = $doc->save();



        $images = $request->file('images');

        foreach( $images as $item){
            $inputFileName = $item->getClientOriginalName();
            $item->move("uploads",$inputFileName);

            $image = new images();
            $image->url = url('uploads/' . $inputFileName);
            $image->docid = $doc->docid;
            $image->save();



        }

        if($docStatus)
        $request->session()->flash('success','Document Added.');
        else
            $request->session()->flash('error','Sorry an error occurred');

        return redirect('home');



    }


    public function viewDoc($id)
    {
        $viewDoc = document::find($id);
        $images  = images::where('docid', $id)->get();

        return view('viewDoc',[
            'viewDoc' => $viewDoc,
            'images' => $images

        ]);
    }
}
