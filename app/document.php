<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class document extends Model
{
    public $table = 'docs';
    
    protected $primaryKey ='docid';
    
    public function category()
    {
        return $this->belongsTo('App\category', 'catid');
    }

    public function images()
    {
        return $this->hasMany('App\images', 'docid');
    }
}
