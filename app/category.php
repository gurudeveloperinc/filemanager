<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    public $table = 'categories';

    protected $primaryKey = 'catid';

    public function docs()
    {
         return $this->hasMany('App\docs', 'docid');
    }
}


