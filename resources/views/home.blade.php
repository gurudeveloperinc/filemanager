@extends('layouts.app')

@section('content')

    <style>
        .well{
            margin-right:5px !important;
            width:90% !important;
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">


                </div>

                <div class="row">
                    <div class="col-md-4 well well-lg">

                        <form>
                            <label>Search:</label>
                            <input type="text" class="form-control" name="search">
                        </form>

                    </div>

                    <div class="col-md-4 col-md-offset-4 well well-lg">{{count($docs)}} Files </div>

                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Found Documents</div>

                    <div class="panel-body">




                        </table>

                        <table id="table" class="table table-hover table-stripped  ">

                            <tr>
                                <th>File Name</th>
                                <th>File Category</th>
                                <th>Uploaded</th>

                            </tr>


                            <tr class="table table-striped">
                                <td>
                                    @foreach( $docs as $doc)

                                        <a href="/home/{{$doc->docid}}">  {{$doc->name}} </a>  <br>

                                    @endforeach
                                </td>

                                <td>
                                    @foreach( $docs as $doc)
                                   {{$doc->category->name}} <br>
                                        @endforeach


                                <td>
                                    @foreach($docs as $doc)

                                    {{$doc->created_at->toFormattedDateString()}} <br>

                                    @endforeach
                                </td>




                            </tr>




                        </table>



                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection